import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListadestinosComponent } from './components/listadestinos/listadestinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMainInfoComponentComponent } from './components/vuelos/vuelos-main-info-component/vuelos-main-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasRoutingModule } from './reservas/reservas-routing.module';

const routesVuelos: Routes = [
  {path: 'main', component: VuelosMainComponentComponent},
  {path: 'mas-info', component: VuelosMainInfoComponentComponent},
  {path: 'vuelos/:id', component: VuelosDetalleComponentComponent},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

const routes: Routes = [
  {path: 'home', component: ListadestinosComponent},
  {path: 'destino/:id', component: DestinoDetalleComponent},
  {path: 'login', component: LoginComponent},
  {path: 'protected', component: ProtectedComponent, canActivate: [ UsuarioLogueadoGuard]},
  {path: 'vuelos', component: VuelosComponentComponent, canActivate: [ UsuarioLogueadoGuard], children: routesVuelos},
  {path: '**', pathMatch: 'full', redirectTo: 'home'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    ReservasRoutingModule
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
