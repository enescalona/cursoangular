import { BehaviorSubject, Subject } from 'rxjs';
import { DestinoViaje } from './destino-viaje.models';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Store } from '@ngrx/store';
import { AppState, APP_CONFIG, AppConfig, db } from '../app.module';
import { NuevoDestinoAction } from './destinos-viajes-state.model';
import { Inject, forwardRef, Injectable } from '@angular/core';

@Injectable()
export class DestinosApiClient{
    destinos: DestinoViaje[];
    current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);

    constructor( private store: Store<AppState>, @Inject(forwardRef(()=>APP_CONFIG)) private config: AppConfig, private http: HttpClient){
        this.store.select(state => state.destinos)
                  .subscribe((data) => {
                      console.log('destinos sub stores');
                      console.log(data);
                      this.destinos = data.items;
                  });
                  // tslint:disable-next-line:align
                  this.store.subscribe((data) => {
                          console.log('all store');
                          console.log(data);
                      });
        this.destinos = [];
    }

    add( d: DestinoViaje ){
        const header: HttpHeaders  = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', this.config.apiEndpoint + '/my', { nuevo: d.nombre }, { headers: header });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if( data.status === 200 ){
                this.store.dispatch(new NuevoDestinoAction(d));
                const myDb = db;
                myDb.destinos.add(d);
                console.log('todos los destinos');
                myDb.destinos.toArray().then(destinos => console.log(destinos));
            }
        });
    }

    getAll(): DestinoViaje[]{
        return this.destinos;
    }

    getById( id: string ): DestinoViaje{

        return this.destinos.filter( function( d ){ return d.id.toString() === id; } )[0];
    }

    elegir(d: DestinoViaje){
        this.destinos.forEach(x=>x.setSelected(false));
        d.setSelected(true);
        this.current.next(d);
    }

    subscribeOnChange(fn){
        this.current.subscribe(fn);
    }
}
