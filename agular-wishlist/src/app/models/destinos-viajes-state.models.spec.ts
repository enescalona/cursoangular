import {
    reduceDestinosViajes,
    DestinoViajesState,
    InitMyDataAction,
    intializeDestinosViajesState,
    NuevoDestinoAction
} from './destinos-viajes-state.model';
 
import { DestinoViaje } from './destino-viaje.models';

describe('reduceDestinosViajes', () => {
    it( 'should reduce init data', () => {
        const prevState: DestinoViajesState = intializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        const newState: DestinoViajesState = reduceDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });

    it( 'should reduce new item added', () => {
        const prevState: DestinoViajesState = intializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction( new DestinoViaje('barcelona', 'url'));
        const newState: DestinoViajesState = reduceDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });
});