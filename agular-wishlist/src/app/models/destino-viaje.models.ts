export class DestinoViaje{

    private selected: boolean;
    public servicios: string[];
    constructor(public nombre: string, public imagenUrl: string, public id?: string, public votes?: number){
        this.servicios = ['pileta', 'desayuno'];
        this.votes = 0;
    }

    isSelected(){
        return this.selected;
    }

    setSelected(s: boolean){
        this.selected = s;
    }

    voteUp(){
        this.votes++;
    }

    voteDown(){
        this.votes--;
    }
}