import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListadestinosComponent } from './components/listadestinos/listadestinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { DestinosApiClient } from 'src/app/models/destinos-api-client.model';
import { DestinoViajesState, intializeDestinosViajesState, reduceDestinosViajes, DestinosViajesEffects, InitMyDataAction } from './models/destinos-viajes-state.model';
import { StoreModule as NgRxStoreModule, ActionReducerMap, Store } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMainInfoComponentComponent } from './components/vuelos/vuelos-main-info-component/vuelos-main-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import { DestinoViaje } from './models/destino-viaje.models';
import  Dexie  from 'dexie';
import { TranslateService, TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';







//app config
export interface AppConfig {
  apiEndpoint: string;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');


//redux init
export interface AppState{
  destinos: DestinoViajesState;
}

const reducers: ActionReducerMap<AppState> = {
  destinos: reduceDestinosViajes
};

let reducersInitialState = {
  destinos: intializeDestinosViajesState()
};


//redux fin init

//app init
export function init_app(appLoadService: AppLoadService):() =>Promise<any>{
  return () => appLoadService.intializeDestinosViajesState();
}
@Injectable()
class AppLoadService{
  constructor( private store: Store<AppState>, private http: HttpClient){}
  async intializeDestinosViajesState(): Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad'});
    // tslint:disable-next-line:object-literal-shorthand
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers: headers});
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
//end appp init

//dexie db
export class Translation{
  constructor( public id: number, public lang: string, public key: string, public value: string){}
}
@Injectable({
  providedIn: 'root'
})
export class MyDatabase extends Dexie{
  destinos: Dexie.Table<DestinoViaje, number>;
  translations: Dexie.Table<Translation, number>;
  constructor(){
    super('MyDatabase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenUrl',
      translations: '++id, lang, key, value',
    });
  }
}
export const db = new MyDatabase();
// end dixie db

//i18n ini
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient){}
  getTranslation(lang: string): Observable<any>{
    const promise = db.translations
                      .where('lang')
                      .equals(lang)
                      .toArray()
                      .then(results => {
                          if(results.length === 0){
                            return this.http
                                       .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint + '/api/translation?lang=' + lang )
                                       .toPromise()
                                       .then(apiResult => {
                                         db.translations.bulkAdd(apiResult);
                                         return apiResult;
                                       });
                          }
                          return results;
                      }).then((traducciones)=>{
                        console.log('traducciones cargadas:');
                        console.log(traducciones);
                        return traducciones;
                      }).then((traducciones) => {
                        return traducciones.map((t) => ({ [t.key]: t.value }));
                      });
    return from(promise).pipe( flatMap((elems) => from(elems)));
  }
}

function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}

//end i18n ini

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListadestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMainInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      }}),
      EffectsModule.forRoot([DestinosViajesEffects]),
      StoreDevtoolsModule.instrument(),
      ReservasModule,
      HttpClientModule,
      NgxMapboxGLModule,
      BrowserAnimationsModule,
      TranslateModule.forRoot({
        loader: {
          provide: TranslateLoader, useFactory: (HttpLoaderFactory), deps: [HttpClient]
        }
      })
  ],
  providers: [
    AuthService,
    TranslateService,
    UsuarioLogueadoGuard,
    {provide: APP_CONFIG, useValue: APP_CONFIG_VALUE},
    AppLoadService,
    {provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true},
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
