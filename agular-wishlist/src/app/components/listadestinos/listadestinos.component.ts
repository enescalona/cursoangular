import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinosApiClient } from '../../models/destinos-api-client.model';
import { DestinoViaje } from '../../models/destino-viaje.models';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction } from 'src/app/models/destinos-viajes-state.model';
import { NuevoDestinoAction } from '../../models/destinos-viajes-state.model';

@Component({
  selector: 'app-listadestinos',
  templateUrl: './listadestinos.component.html',
  styleUrls: ['./listadestinos.component.css'],
  providers: [
    DestinosApiClient
  ]
})
export class ListadestinosComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  constructor( public destinosApiClient: DestinosApiClient, public store: Store<AppState> ) {
    this.onItemAdded = new EventEmitter(); 
    this.updates = [];
    this.store.select( state => state.destinos.favorito ).subscribe( d =>{
      if( d!= null ){
        this.updates.push('Se ha elegido ' + d.nombre);
      }
    } );

  }

  ngOnInit(): void {
  }

  agregado(d: DestinoViaje){

    //this.destinos.push( new DestinoViaje(d.nombre, d.imagenUrl) );
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido( e: DestinoViaje){
    this.destinosApiClient.elegir(e);
    this.store.dispatch(new ElegidoFavoritoAction(e));
  }

}
