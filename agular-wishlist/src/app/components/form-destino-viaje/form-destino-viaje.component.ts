import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';
import { DestinoViaje } from '../../models/destino-viaje.models';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLogitud = 3;
  searchResult: string[];

  constructor( fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValdator,
        this.nombreValidatorParametrizable( this.minLogitud )
      ])],
      url: ['']
    });

    this.fg.valueChanges.subscribe((form: any) => {
      console.log('Cambio el formulario: ', form);
    })
  }

  ngOnInit(): void {
    let elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input').pipe( 
      map( (e: KeyboardEvent) => (e.target as HTMLInputElement).value ),
      filter( text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap( (text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
     ).subscribe( ajaxResponse => {
       this.searchResult = ajaxResponse.response;
     });
  }

  guardar( nombre: string, url: string): boolean{
      const d = new DestinoViaje( nombre, url );
      this.onItemAdded.emit(d);
      return false;
  }

  nombreValdator(controls: FormControl):{[s: string]: boolean}{
    const l = controls.toString().trim().length;
    if( l > 0 && l < 5){
      return { invalidNombre: true };
    }
    return null;
  }

  nombreValidatorParametrizable( minLong: number): ValidatorFn{
    return ( controls: FormControl ): {[s: string]: boolean} | null =>{
      
      const l = controls.toString().trim().length;
      if( l > 0 && l < minLong){
        return { minLongNombre: true };
      }
      return null;
    };
  }

}
