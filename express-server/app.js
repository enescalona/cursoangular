let express = require('express'), corns = require('cors');
let app = express();
app.use(express.json());
app.use(corns());
app.listen(3000, () => console.log('Server running on port 3000'));

let ciudades = ["Paris", "Barcelona", "Barranquilla", "Montevideo", "Mexico DF"];
app.get("/ciudades", (req, res, next) => res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase())>-1)));

let miDestinos = [];
app.get("/my", (req, res, next) => res.json(miDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    miDestinos = req.body;
    res.json(miDestinos);
});

app.get("/api/translation", (req, res, next) => res.json([
        {lang: req.query.lang, key: 'HOLA', value: 'HOLA ' + req.query.lang}
    ]));